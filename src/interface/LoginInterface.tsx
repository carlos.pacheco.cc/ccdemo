
export interface LoginResp {
    status: string;
    data:   LoginData;
}

export interface LoginData {
    user:    User;
    session: Session;
}

export interface Session {
    tokenType:    string;
    accessToken:  string;
    expiresOn:    number;
    refreshToken: string;
}

export interface LoginFields {
    email:string,
    password:string,
    clientType: string
}

export interface AllUsers {
    status: string;
    data:   Data;
}

export interface Data {
    totalCount: number;
    users:      User[];
}

export interface User {
    id:         number;
    actived?:    boolean;
    blocked?:    boolean;
    role?:       Role;
    appleId?:    string;
    googleId?:   null | string;
    facebookId?: null | string;
    avatarId?:   null | string;
    language?:   Language;
    firstName:  string;
    lastName?:   string;
    email?:      string;
    phone?:      string;
    dob?:        string;
    deleted?:    boolean;
    createdAt?:  string;
    lastLogin?:  string;
}

export interface SignupFields {
    "email": string,
    "password": string,
    "firstName": string,
    "lastName": string,
    "phone": string,
    "avatar"?: {}
    clientType : string
    
}
export enum Language {
    En = "en",
}

export enum Role {
    Admin = "ADMIN",
}

