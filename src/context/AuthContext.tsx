import React, { useEffect, useReducer } from "react";
import { createContext } from "react";
import { useAuth,} from "../hooks/useAuth";
import { LoginResp, LoginData, User, Session } from '../interface/LoginInterface';
import { EditResponse, EditFields } from '../interface/EditInterface';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useForm } from '../hooks/useForm';

export interface AuthState{
    isLoggedIn: boolean,
    userData: User,
    session: Session,
    token: string
}

export const authInitialState : AuthState = {
    isLoggedIn:false,
    userData: {
        firstName: "",
        lastName: "",
        phone: "",
        email: "",
        dob: "",
        id: 0,
    },
    session: {
        tokenType:"",
        accessToken:"",
        refreshToken:"",
        expiresOn:0
    },
    token:""
}

export interface AuthContextProps {
    authState : AuthState,
    login : (resp: LoginResp, remember:boolean) => void,
    edit : (resp : EditFields) => void,
    checkToken: () => Promise<boolean>,
    logout: () => void,
}

export const AuthContext = createContext({} as AuthContextProps);

export const AuthProvider = ({children}: any) => {

    const [authState, dispatch,] = useReducer(useAuth, authInitialState)

    const {get, response} = useForm<{}, User>("", {}, )

 
    const checkToken = async() : Promise<boolean>=>{

        const token = await AsyncStorage.getItem("token")
        let resp : AuthState
        if(token){
            const userResp = await get()
            if(userResp){
                resp = {
                    ...authState,
                    userData:userResp,
                    token:token,
                }
                dispatch({type:"LoggedIn", payload: resp})
                return true
            }
            
        }
        return false

    }

    const login = async (resp : LoginResp, remember:boolean) => {

        dispatch({type:"Login", payload:resp})
        if(remember){
            await AsyncStorage.setItem("token", resp.data.session.tokenType + " " + resp.data.session.accessToken)
        }
        

    }
    const edit = (resp : EditFields) => {
        dispatch({type:"Edit", payload:resp})
    }

    const logout = async () => {
        await AsyncStorage.removeItem("token")
        dispatch({type:"Logout"})
    }

    return(
        <AuthContext.Provider
            value={{
                authState,
                login,
                edit,
                checkToken,
                logout
            }}
        >
            {children}
        </AuthContext.Provider>
    )
}