
export const colors = {
    background: "#FFF",
    text: "#838383",
    red:"#E6403A",
    blue:"#4889F2",
    form:"#fafafa",
    formText:"#a5a5a5",
    checkBox:"#d8d8d8",
    underLineText:"rgba(165, 165, 165, .8)",
    listText: "#aeaead",
    cameraButton : "rgba(131, 131, 131, .5)"
}