import { StyleSheet } from "react-native";
import { colors } from './colors';


export const styles = StyleSheet.create({
    globalMargin:{
        marginHorizontal:28
    },
    screenWhite: {
        flex: 1,
        backgroundColor: "white"
    },
    presentationTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: colors.blue
    },
    presentationSubTitle: {
        fontSize: 16,
    },
    title:{
        fontSize: 20, 
        marginBottom: 10, 
        color:colors.text,
        fontWeight:"bold"
    },
    scrollContainer: {
        flex: 1,
        backgroundColor: "white"
    },
    contentScroll: {
        flexGrow: 1,
        justifyContent: "space-between",
        paddingHorizontal: 28,
        paddingBottom:5,
        alignItems: "stretch"
    },
    logo: {
        width: 125,
        height: 125,
        alignSelf: "center",
        marginTop:30,
        marginBottom:20
    },
    formImputContainer:{
        width: "100%",
        marginBottom:17,
        height:45,
        borderWidth: 1,
        borderColor:"rgba(0, 0, 0, .3)",
        color:colors.formText,
        borderRadius: 5,
        backgroundColor:"purple"
    },
    formImputText: {
        fontWeight:"bold",
        width:"100%",
        paddingHorizontal:20,
        color:"black"
    },
    signUpContainer: {
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "center",
    },
    checkBoxFill: {
        position:"absolute", 
        left:6,
        top:6, 
        backgroundColor:colors.checkBox, 
        width:17, 
        height:17,
        borderWidth:.5,
        borderColor:"black"
    },
    underlineText: {
        color: colors.underLineText,
        fontSize:12,
        fontWeight:"bold",
        paddingBottom:5,
        top:3,
        borderBottomWidth:1,
        borderBottomColor: colors.underLineText,
    },
    Button:{
        backgroundColor: colors.red,
        height: 40,
        marginVertical: 24,
        borderRadius: 3,
        alignItems: "center",
        justifyContent: "center"
    },
    buttonText: {
        fontSize: 16,
        color: "white",
        textTransform: "uppercase"
    },
    borderListContainer:{
        justifyContent: "space-between",
        borderBottomColor: colors.listText,
        borderBottomWidth: 2,
        flexDirection: "row",
        alignItems:"center",
        marginBottom:15
    },
    borderListTitle: {
        fontSize: 16,
        marginBottom: 15,
        marginHorizontal: 5,
        color:("rgba(0, 0, 0, .7)"),
        fontWeight:"800"
    },
    globalButton: {
        width: "100%",
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center"
    },
    globalButtonText:{
        color:"white",
        fontSize: 20,
        marginVertical: 10
    },
    settingsTitle:{
        alignItems:"center",
        height:50,
        marginTop: 20,
        marginBottom:35,
        alignSelf:"center",
        fontSize:16,
        fontWeight:"900"
    }   
    
});