import React from 'react'
import { Modal, StyleSheet, Text, View, Button } from 'react-native';
import { TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import { ImagePickerResponse, launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { colors } from '../themes/colors';
import { useState } from 'react';
import { Image } from 'react-native';
import { styles } from '../themes/globalTheme';

interface Props {
    launchResp: (data: ImagePickerResponse) => void
}

export const CameraButton = ({launchResp} : Props) => {
    const [opened, setopened] = useState(false)
    const [selected, setselected] = useState<ImagePickerResponse>()

    const takePhoto = () => {
        launchCamera({
            mediaType: "photo",
            quality: 1,
            
        }, (resp) => {
            if(!resp.didCancel){
                setselected(resp)
                launchResp(resp)
            }
        })
    }

    const takePhotoFromGalery = () => {
        launchImageLibrary({
            mediaType: "photo",
            quality: .5
        }, (resp) => {
            if(!resp.didCancel){
                setselected(resp)
                launchResp(resp)
            }
        }
        )
    }

    return (
        <TouchableOpacity 
            style={{alignSelf:"center",}}
            onPress={() => setopened(true)}
        >
            <View style={{
                borderRadius:100,
                borderWidth:4,
                width:75,
                height:75,
                alignItems:"center",
                justifyContent:"center",
                borderColor:colors.cameraButton,
                marginBottom:20
            }}>
                {
                    (selected && !opened) 
                    ?
                    <View>
                        <Image
                            source={{
                                uri:selected.assets[0].uri
                            }}
                            style={{
                                width:38,
                                height:38
                            }}
                        />
                        
                    </View>
                    
                    :
                    <Icon
                        name="camera-outline"
                        size={38}
                        color={colors.cameraButton}
                        
                    />

                }
                
            </View>
            <Modal
                visible={opened}
                animationType="slide"
                transparent={true}
            >
                <TouchableOpacity 
                    style={{backgroundColor:"rgba(0,0,0,.3)", flex:1}}
                    onPress={() => setopened(false)}
                >
                    
                </TouchableOpacity>
                <View style={{
                            alignItems:"center",
                            justifyContent:"space-around",
                            flex:1, 
                            backgroundColor:"white",
                            flexDirection:"row" 
                        }}>
                    {
                        (!selected?.assets) 
                        ?
                        <View style={{
                            justifyContent:"space-around",
                            flex:1,
                            flexDirection:"row"
                        }}>
                            <TouchableOpacity
                                style={{alignItems:"center"}}
                                onPress={takePhoto}
                            >
                                <Icon
                                    name="camera"
                                    size={100}
                                />
                                <Text>TAKE PHOTO</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{alignItems:"center"}}
                                onPress={takePhotoFromGalery}
                            >
                                <Icon
                                    name="image"
                                    size={100}
                                />
                                <Text>OPEN GALLERY</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{
                            ...styles.globalMargin,
                            alignItems:"center",
                            flex:1, 
                            backgroundColor:"white",
                            
                        }}>
                            <View>
                                <Image
                                    source={{
                                        uri:selected.assets[0].uri
                                    }}
                                    style={{width:200, height:200}}
                                />
                                <TouchableOpacity
                                    style={{
                                        position:"absolute",
                                        right:0
                                    }}
                                    onPress={() => setselected(undefined)}
                                >
                                    <Icon
                                        name={"close"}
                                        size={30}
                                        color="red"
                                    />
                                </TouchableOpacity>
                                
                            </View>
                            
                            <View style={{
                                width:"100%",
                                flexDirection:"row",
                                justifyContent:"space-evenly",
                                alignItems:"flex-end",
                            }}>
                                <TouchableOpacity 
                                    style={{
                                        ...styles.Button, 
                                        width:100,
                                        marginBottom:0
                                    }}
                                    onPress={() => {
                                        setopened(false)
                                        setselected(undefined)
                                    }}
                                    >
                                    <Text style={styles.buttonText}>
                                        CANCEL
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{
                                        ...styles.Button, 
                                        backgroundColor:colors.blue, 
                                        width:100,
                                        marginBottom:0
                                    }}
                                    onPress={() => setopened(false)}
                                >
                                    <Text style={styles.buttonText}>
                                        Accept
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            
                        </View>  
                    }
                </View>

            </Modal>
            
        </TouchableOpacity>
    )
}

