import { TextInput, TouchableOpacity, Text } from 'react-native';
import { styles } from '../themes/globalTheme';
import React, { useRef, useState } from 'react';
import { View } from 'react-native';
import { Image } from 'react-native';
import { useEffect } from 'react';
import { KeyboardTypeOptions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { colors } from '../themes/colors';

export enum FormType {
    email = "email",
    password = "password",
    name = "name",
    default = "",
    confirmPassword = "confirmPassword",
    number = ""
}

export interface InputProps {
    placeholder: string,
    formType?: FormType,
    onChangeText: (text : string, info : string) => void
    defaultValue?: string
    compare?: string
    onFocus?: () => void
}

export const FormInput = ({placeholder, onChangeText, formType=FormType.default, defaultValue="", compare="", onFocus}: InputProps) => {

    const [textVisible, settextVisible] = useState(formType != FormType.password && formType != FormType.confirmPassword)
    const text = useRef("")
    const [info, setinfo] = useState("")
  
    const getKeyboardType = () : KeyboardTypeOptions => {
        let keyboardType : KeyboardTypeOptions = "default"
        switch(formType){
            case FormType.email:
                keyboardType="email-address"
                break;
            case FormType.name:
                keyboardType="name-phone-pad"
                break;
            case FormType.password:
                keyboardType="default"
                break
            default:
                keyboardType="default"
                break
        }
        return keyboardType
    }

    const checkText = (textInput : string, validate : boolean = false,) => {
        text.current = textInput
        let avise = ""
        if(formType === FormType.password){
            const regex = /[0-9]/;
            
            if(textInput.length < 8){
                avise = "La contraseña debe tener al menos 8 caracteres"
            }
            else if(!regex.test(textInput)){
                avise = "La contraseña debe contener al menos un numero"
            }
        }else if(formType === FormType.email){
            const regex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
            if(!regex.test(textInput)){
                avise = "Introduce una direccion de correo valida"
            }
        }else if(formType === FormType.confirmPassword){
            if(!(textInput == compare)){
                avise = "Las contraseñas no coinciden"
            }
        }else if((formType === FormType.name) || (formType=== FormType.number)){
            if(textInput === ""){
                avise = "Este dato es obligatorio"
            }
        }
        if((info != avise && validate) || (info != "")){
            setinfo(avise)
        }
    }
    return (
        <View style={styles.formImputContainer}>
            <View style={{
                flexDirection:"row", backgroundColor:colors.form
            }}>
                <TextInput 
                    placeholder={placeholder}
                    placeholderTextColor="rgba(0, 0, 0, .4)"
                    autoCapitalize="none"
                    onChangeText= {(text) => {
                        checkText(text)
                        onChangeText(text, info)
                    }}
                    onEndEditing={() => checkText(text.current, true)}
                    textContentType= "password"
                    secureTextEntry= {(text.current != "") ? !textVisible : undefined}
                    defaultValue={defaultValue}
                    autoCorrect={false}
                    keyboardType={getKeyboardType()}
                    style={styles.formImputText}
                    numberOfLines={1}
                    onFocus={onFocus}
                />
                
                { (formType === FormType.password || formType === FormType.confirmPassword) && <PasswordVisible
                    onPress = {() => settextVisible(!textVisible)}
                    visible = {textVisible}
                />}
            </View>
            
            <Text style={{color:"red", fontSize:10, marginVertical:0}}>{info}</Text>
            
            
            
        </View>
        
    )
}

interface passProps {
    onPress : () => void,
    visible: boolean
}

const PasswordVisible = ({onPress, visible} : passProps) => {

    return(
        <TouchableOpacity
            style={{
                position:"absolute",
                right: 10,
                justifyContent: "center",
                height:"100%",
            }}
            onPress={onPress}
        >
            <Icon
                name={visible ? "eye-off" : "eye"}
                size={20}
                color="rgba(.5, .5, .5, .3)"
            />
        </TouchableOpacity>
    )
}
