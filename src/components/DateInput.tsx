import React from 'react'
import { useState } from 'react';
import { View, TouchableOpacity, Modal, Button } from 'react-native';
import DatePicker from 'react-native-date-picker';
import { FormInput, InputProps } from './FormInput';
import { colors } from '../themes/colors';
import { styles } from '../themes/globalTheme';
import { Text } from 'react-native';
interface DateProps extends InputProps {
    initialDate : string
}

export const DateInput = ({placeholder, initialDate} : DateProps) => {
    const [date, setdate] = useState(new Date)
    const [focus, setfocus] = useState(false)
    const [dateText, setdateText] = useState<string>(initialDate)

    return (
        <View>
            <TouchableOpacity
                onPress={() => setfocus(true)}
            >
                <FormInput
                placeholder={placeholder}
                defaultValue={dateText}
                onChangeText={() => {}}
                onFocus={() => setfocus(true)}
                />
                <View
                    style={{
                        position:"absolute",
                        width:"100%",
                        height:"100%",
                        backgroundColor:"rgba(0,0,0,0)"
                    }}
                />
            </TouchableOpacity>
         
        {
            focus &&
            <Modal
                animationType="slide"
                transparent={true}
                visible={true}
            >
                <View
                    style={{backgroundColor:"rgba(0,0,0,.3)", flex:1}}
                />
                <View style={{backgroundColor:colors.form, paddingVertical:20, alignItems:"center"}}>
                    <DatePicker
                        date={date}
                        onDateChange={(date) => setdate(date)}
                        mode={"date"}
                    />
                    <View style={{
                        flexDirection:"row",
                        justifyContent:"space-around",
                        width:"100%"
                    }}>
                        <TouchableOpacity 
                            style={{
                                ...styles.Button,
                                width:"40%",
                                marginBottom:0
                            }}
                            onPress={() => setfocus(false)}
                        >
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={{
                                ...styles.Button,
                                width:"40%",
                                marginBottom:0,
                                backgroundColor:colors.blue
                            }}
                            onPress={() => {
                                setdateText(date.toLocaleDateString())
                                setfocus(false)
                            }}
                        >
                            <Text style={styles.buttonText}>Accept</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                    
            </Modal>
        }
        </View>
        
    )
}
