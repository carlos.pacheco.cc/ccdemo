import React from 'react'
import { Text } from 'react-native';
import { View } from 'react-native';
import { styles } from '../themes/globalTheme';

interface Props {
    title : string,

}

export const TextList = ({title} : Props) => {
    return (
        <View style={styles.borderListContainer}>
            <Text style={styles.borderListTitle}>{title}</Text>
        </View>
    )
}

