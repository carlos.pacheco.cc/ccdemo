import React from 'react'
import { View } from 'react-native'
import { colors } from '../themes/colors'
import { styles } from '../themes/globalTheme'
import Checkbox from '@react-native-community/checkbox';

interface Props {
    value:boolean,
    setValue:(newValue:boolean) => void
}

export const CheckBox = ({value, setValue} : Props) => {
    return (
        <View style={{width:20, marginRight:10}}>
            <View 
                style={{
                    ...styles.checkBoxFill, 
                    backgroundColor:(value) ? "black" : colors.checkBox, 
                }}
            />
            <Checkbox
                value={value}
                onValueChange={(newValue) => setValue(newValue)}
                tintColors={{true:colors.checkBox, false:colors.checkBox}}
            />
            
        </View>
    )
}
