import React from 'react'
import { Text, TouchableOpacity } from 'react-native';
import { styles } from '../themes/globalTheme';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props  {
    text : string,
    onPress : () => void
}

export const TouchableText = ({text, onPress} : Props) => {
    return (
        <TouchableOpacity 
            style={{
                ...styles.borderListContainer,
                marginBottom:20
            }}
            onPress={() => onPress()}
        >
            <Text style={styles.borderListTitle}>{text}</Text>
            <Icon
                name="chevron-forward-outline"
                size={15}
                style={styles.borderListTitle}
            />
        </TouchableOpacity>
    )
}
