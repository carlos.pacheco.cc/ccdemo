import { StackScreenProps } from '@react-navigation/stack'
import React, { useEffect } from 'react'
import { Text } from 'react-native';
import { View, Button, StyleSheet, TouchableOpacity } from 'react-native';
import { FormInput } from '../components/FormInput';
import { styles } from '../themes/globalTheme';

interface Props extends StackScreenProps<any, any>{};

export const ResetPasswordScreen = ({navigation} : Props) => {

    return (
        <View
            style={styles.globalMargin}
        >
            <Text style={{
                ...styles.settingsTitle,
                marginBottom:10
                }}>
                RECOVERY PASSWORD
                </Text>
            <FormInput
                placeholder="Email"
                onChangeText={() => {}}
            ></FormInput>
            <TouchableOpacity style={{
                ...styles.Button,
                marginTop:10
            }}>
                <Text style={styles.buttonText}>Send</Text>
            </TouchableOpacity>
        </View>
    )
}

