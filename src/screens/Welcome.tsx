import React, { useEffect } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { StackScreenProps } from '@react-navigation/stack';
import { colors } from '../themes/colors';
import { styles } from '../themes/globalTheme';

interface Props extends StackScreenProps<any, any>{};

export const Welcome = ({navigation} : Props) => {

    useEffect(() => {
        navigation.setOptions({
            header: () => null
        })
    }, [])

    return (
        <View style={{
            ...styles.globalMargin,
            flex:1
            }}>
            <View style={{
                flex:1,
                justifyContent:"center"
            }}>
                <Image
                    source={{
                        uri: "https://ort.hiringroomcampus.com/assets/media/ort/company_609070654b8e56648d6edef1.png"
                    }}
                    style={{
                        ...styles.logo,
                        width:200,
                        height:200,
                        
                    }}
                ></Image>
            </View>

            <View style={{
                flexDirection:"row",
                justifyContent:"space-between"
            }}>
                    <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            width: 140,
                            backgroundColor:colors.blue
                        }}
                        onPress={() => navigation.navigate("Signup")}
                    >
                        <Text style={styles.buttonText}>SIGNUP</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            backgroundColor: colors.red,
                            width: 140,
                        }}
                        onPress={() => navigation.navigate("Login")}    
                    >
                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableOpacity>
            </View>
        </View>
    )
}
