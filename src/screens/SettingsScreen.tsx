import React, { useEffect } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { StackScreenProps } from '@react-navigation/stack';
import { TouchableText } from '../components/TouchableText';
import { styles } from '../themes/globalTheme';
import { Text } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';


interface Props extends StackScreenProps<any, any>{};

export const SettingsScreen = ({navigation} : Props) => {

    const {logout} = useContext(AuthContext);

    return (
        <View style={{flex:1}}>
            <View style= {styles.globalMargin}>
                <View>
                    <Text style={styles.settingsTitle}>SETTINGS</Text>
                </View>
                <TouchableText
                    text="Edit Profile" 
                    onPress={() => navigation.navigate("EditProfileScreen")}
                    />
                <TouchableText 
                    text="Change Password"
                    onPress={() => navigation.navigate("ChangePasswordScreen")} 
                />
                <TouchableText
                    text="Terms & conditions" 
                    onPress={() => {navigation.navigate("TermsScreen")}}
                />
            </View>
            <View style={{
                ...styles.globalMargin,
                justifyContent:"flex-start", 
                flex:1, 
                marginBottom:15, 
                flexDirection:"column-reverse"}}>
                <Text style={{
                    alignSelf:"center",
                    opacity:.5,
                    fontWeight:"bold"
                }}>
                    Version {DeviceInfo.getVersion()}
                </Text>
                <TouchableOpacity
                    onPress={() => logout()}
                    style={{
                        ...styles.Button
                    }}
                >
                    <Text style={styles.buttonText}>Logout</Text>
                </TouchableOpacity>
            </View>
            
        </View>
        
    )
}
