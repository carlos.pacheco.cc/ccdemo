import React, { useEffect, useState,} from 'react'
import { Image, Text, TouchableOpacity, View, SafeAreaView, Platform, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { styles } from '../themes/globalTheme';
import { FormInput, FormType } from '../components/FormInput';
import { HeaderBackButton, StackScreenProps } from '@react-navigation/stack';
import { formState, useForm} from '../hooks/useForm';
import { SignupFields } from '../interface/LoginInterface';
import { CameraButton } from '../components/CameraButton';
import { ImagePickerResponse } from 'react-native-image-picker';
import { CheckBox } from '../components/CheckBox';
import { DateInput } from '../components/DateInput';

interface Props extends StackScreenProps<any, any>{};

export const Signup = ({navigation} : Props) => {
    const {formData, form, checkForm, post, sucess} = useForm<SignupFields, {}>("/signup", {clientType:Platform.OS.toUpperCase()} as SignupFields);
    const [termsCheckbox, settermsCheckbox] = useState(false)

    
    useEffect(() => {
        if(sucess === formState.completed){
            navigation.navigate("Login")
        }
    },)

    const imageResponse = (data:ImagePickerResponse) =>{
        if(data.assets[0].uri){
            checkForm({
                uri: data.assets[0].uri,
                type: data.assets[0].type,
                name: data.assets[0].fileName
            },"", formData.avatar)
        }
    }

    return (
        <SafeAreaView style={{flex:1,}}>
            <ScrollView 
                style={styles.scrollContainer} 
                contentContainerStyle={{...styles.contentScroll, paddingTop:0,}}
                keyboardShouldPersistTaps= "always" 
            >    
                <View >
                    <Image 
                        source={{
                            uri: "https://ort.hiringroomcampus.com/assets/media/ort/company_609070654b8e56648d6edef1.png"
                        }}
                        style={{...styles.logo}}
                    />
                </View>
                

                <View style={{...styles.signUpContainer, marginTop:0}}>
                    <Text style={{...styles.title, marginBottom:20}}>Create your Account</Text>
                    <CameraButton
                        launchResp={(resp) => imageResponse(resp)}
                        
                    />
                    <FormInput
                        placeholder="First Name"
                        formType={FormType.name}
                        onChangeText={(text, info) => checkForm(text, info,formData.firstName)}
                    />
                    <FormInput
                        placeholder="Last Name"
                        formType={FormType.name}
                        onChangeText={(text, info) => checkForm(text, info,formData.lastName)}
                    />
                    <FormInput
                        placeholder="Phone Number"
                        onChangeText={(text, info) => checkForm(text, info,formData.phone)}
                        formType={FormType.number}
                    />
                    <DateInput
                        placeholder="Birthday"
                        onChangeText={() => {}}
                        initialDate=""
                    />
                    <FormInput
                        placeholder="Email"
                        onChangeText={(text, info) => checkForm(text, info,formData.email)}
                        formType={FormType.email}
                    />
                    <FormInput
                        placeholder="Password"
                        onChangeText={(text, info) => checkForm(text, info,formData.password)}
                        formType={FormType.password}
                    />
                    <FormInput
                        placeholder="Confirm Password"
                        onChangeText={() => {}}
                        formType={FormType.confirmPassword}
                        compare={form.password}
                    />
                    <View style={{flexDirection:"row", alignItems:"center"}}>
                        <CheckBox
                            value={termsCheckbox}
                            setValue={settermsCheckbox}
                        />
                        <TouchableOpacity
                            onPress={() => navigation.navigate("TermsScreen")}
                        >
                            <Text style={styles.underlineText}>Terms & conditions</Text>
                            
                        </TouchableOpacity>
                    </View>
                    
                    <Text style={{color:"red"}}>
                       {(sucess === formState.error) ? "Error, revisa tus datos" : ""}
                   </Text>

                    {sucess === formState.loading
                    ? <ActivityIndicator size={30} color="grey" style={{marginTop:20}}/>
                    : <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            width:"100%",
                            marginTop:2
                        }}
                        onPress={() => post()}
                    >
                        <Text style={styles.buttonText}>SIGNUP</Text>
                    </TouchableOpacity>
                    }
                   
                    
                </View>
            </ScrollView>
        </SafeAreaView>
        

    )
}
