import React, { useEffect, useState } from 'react'
import { Button, FlatList, TouchableOpacity, View } from 'react-native';
import { StyleSheet } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { Text } from 'react-native';
import { apiDB } from '../api/apiDB';
import { AllUsers, User, LoginData } from '../interface/LoginInterface';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { TextList } from '../components/TextList';
import { useForm, formState } from '../hooks/useForm';
import { colors } from '../themes/colors';
import { styles } from '../themes/globalTheme';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props extends StackScreenProps<any, any>{};

export const HomeScreen = ({navigation} : Props) => {
    const {authState} = useContext(AuthContext);
    const {get, response, sucess} = useForm<{}, AllUsers>("/all", {}, {Authorization : authState.token});
        
    useEffect(() => {
        navigation.setOptions({
            title: "Home",
            headerTitleStyle: {color:"white", fontSize: 20},
            headerTitleAlign: "center",
            headerStyle:{backgroundColor:colors.red},
            headerTransparent:false,
            headerRight: () => (
                <TouchableOpacity 
                    style={{marginRight: 15}}
                    onPress={() => navigation.navigate("SettingsScreen")}
                >
                    <Icon
                        name="settings-sharp"
                        size={18}
                        color="white"
                    />
                </TouchableOpacity>
            )
        })
        get()
    }, [])

    return (
        <View>
            {(sucess === formState.completed) &&
            <FlatList
                data={response?.data.users}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item}) => <TextList title={item.firstName}/>}
                style={{...styles.globalMargin, marginTop:50}}
            />}
        </View>
        
    )
}
