import React, { useEffect } from 'react'
import { TouchableOpacity, View, SafeAreaView } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { FormInput, FormType } from '../components/FormInput';
import { ScrollView } from 'react-native';
import { styles } from '../themes/globalTheme';
import { Text } from 'react-native';
import { useForm } from '../hooks/useForm';
import { colors } from '../themes/colors';

interface Props  extends StackScreenProps<any, any>{};

export const ChangePasswordScreen = ({navigation} : Props) => {

    const {post} = useForm("/password", {});


    return (
        <SafeAreaView>
            <Text style={{
                ...styles.settingsTitle,
                marginBottom:15
            }}>
                CHANGE PASSWORD
            </Text>
            <ScrollView style={styles.globalMargin}>
                <FormInput
                    placeholder="Current Password"
                    onChangeText={() => {}}
                    formType={FormType.password}
                />
                <FormInput
                    placeholder="New Password"
                    onChangeText={() => {}}
                    formType={FormType.password}
                />
                <FormInput
                    placeholder="Confirm New Password"
                    onChangeText={() => {}}
                    formType={FormType.confirmPassword}
                />
                <TouchableOpacity style={{
                    ...styles.Button,
                    backgroundColor:colors.red,
                    width:"100%",
                    marginTop:10,
                }}>
                    <Text style={styles.buttonText}>Update</Text>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>
        
    )
}
