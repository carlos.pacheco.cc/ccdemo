import React from 'react'
import { View, SafeAreaView, ActivityIndicator } from 'react-native';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { useEffect } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

interface Props extends StackScreenProps<any, any>{};

export const LoadingScreen = ({navigation} : Props) => {
    const {checkToken} = useContext(AuthContext)

    useEffect(() => {

        const tokenIsValid = checkToken()
        tokenIsValid.then((resp) => {
            if(!resp.valueOf()){
                navigation.navigate("Welcome")
            }
        })

    },)

    return (
        <SafeAreaView
            style={{
                alignItems:"center",
                justifyContent:"center",
                flex:1
            }}
        >
            <ActivityIndicator
                size={100}
                color="black"
            />
        </SafeAreaView>
    )
}
