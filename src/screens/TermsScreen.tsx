import React, { useEffect } from 'react'
import { View } from 'react-native'
import { StackScreenProps } from '@react-navigation/stack';
import { Text } from 'react-native';
import { styles } from '../themes/globalTheme';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { colors } from '../themes/colors';

interface Props extends StackScreenProps<any, any>{};

export const TermsScreen = ({navigation} : Props) => {

    return (
        <SafeAreaView style={{flex:1}}>
            <View style={{
                ...styles.globalMargin, 
                flex:1
            }}>
                <Text style={{
                    ...styles.settingsTitle,
                    marginBottom:0
                }}
                >
                    TERMS & CONDITIONS
                    </Text>
                
                <Text style={{fontSize: 16}}>Estos son los terminos y condiciones de la app de ccDemo</Text>
                <View style={{
                    bottom:0, 
                    flex: 1, 
                    justifyContent:"flex-end"}}>
                    <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            width:"100%",
                            backgroundColor:colors.red,
                            marginBottom:30
                        }}
                        onPress={() => navigation.pop()}
                    >
                        <Text style={{color:"white"}}>ACCEPT</Text>
                    </TouchableOpacity> 
                </View>
                
            </View>
            
        </SafeAreaView>
    )
}
