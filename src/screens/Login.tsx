import { StackScreenProps } from '@react-navigation/stack';
import React, {} from 'react'
import { Image, Platform, Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FormInput, FormType } from '../components/FormInput';
import { useState } from 'react';
import { formState, useForm } from '../hooks/useForm';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { LoginResp, LoginFields } from '../interface/LoginInterface';
import { ActivityIndicator } from 'react-native';
import { useEffect } from 'react';
import { styles } from '../themes/globalTheme';
import { CheckBox } from '../components/CheckBox';
import { colors } from '../themes/colors';

interface Props extends StackScreenProps<any, any>{};

const initialFields : LoginFields = {
    clientType: Platform.OS.toUpperCase(),
    email: "",
    password:""
}

export const Login = ({navigation} : Props) => {

    const {authState, login} = useContext(AuthContext)
    const {post, sucess, setsucess, form, setform, response} = useForm<LoginFields, LoginResp>("/login", initialFields, {})
    const [rememberState, setrememberState] = useState(false)

    useEffect(() => {
        if(sucess===formState.completed){
            login(response!, rememberState)
            setsucess(formState.finished)
           
        }
    })
    
    return (
        <SafeAreaView style={{flex:1}}>
            <ScrollView 
                style={styles.scrollContainer} 
                contentContainerStyle={{...styles.contentScroll, paddingTop:0}}
                keyboardShouldPersistTaps= "always" 
            >
                <Image
                    source={{
                        uri: "https://ort.hiringroomcampus.com/assets/media/ort/company_609070654b8e56648d6edef1.png"
                    }}
                    style={{
                        ...styles.logo,
                        bottom:10
                    }}
                />
                <View style={{marginTop:10,}}>
                        <Text 
                            style={{
                                ...styles.title,
                                marginBottom:20
                            }}
                        >Login to your Account</Text>
                        <FormInput
                            placeholder={"Email"}
                            formType={FormType.email}
                            onChangeText={(text) => setform({
                                ...form,
                                email: text
                            })}
                        />
                        <FormInput
                            placeholder={"Password"}
                            formType={FormType.password}
                            onChangeText={(text) => setform({
                                ...form,
                                password: text
                            })}
                        />
                        <View style={{
                            flexDirection:"row",
                            alignItems:"center",
                        }}>
                            <CheckBox
                                value={rememberState}
                                setValue={setrememberState}
                            />
                            <Text style={{...styles.underlineText, borderBottomWidth:0}}>Remember me</Text>
                        </View>
                        
                        {
                            sucess === formState.loading
                            ? <ActivityIndicator size={30} color="grey" />
                            :
                            <View style={{width:"100%"}}>
                                <TouchableOpacity
                                    style={{
                                        ...styles.Button,
                                        width:"100%",
                                        backgroundColor:colors.red,
                                        marginBottom:5,
                                        marginTop:15
                                    }}
                                    onPress={() => post()}
                                >
                                    <Text style={styles.buttonText}>Login</Text>
                                </TouchableOpacity>
                                
                    
                                <TouchableOpacity
                                    onPress={() => navigation.navigate("ResetPasswordScreen")}
                                    style={{
                                        alignSelf:"center",
                                    }}
                                >
                                    <Text style={styles.underlineText}>Forgot password?</Text>
                                </TouchableOpacity>
                                <Text style={{color:"red"}}>{(sucess === formState.error ? "Error, revisa tus datos" : "")}</Text>
                            </View>
                        }
                        
                        
                </View>
                <View style={{flex:1, justifyContent:"flex-end", paddingHorizontal:10}}>
                    <View style={{flexDirection:"row", alignItems:"center"}}>
                        <View style={{
                            borderBottomColor:"rgba(0, 0, 0, .3)", 
                            borderBottomWidth:1, 
                            flex:1, 
                        }}/>
                        <Text style={{
                            color:"rgba(0, 0, 0, .3)",
                            marginHorizontal:20
                        }}>or signin with</Text>
                        <View style={{
                            borderBottomColor:"rgba(0, 0, 0, .3)",
                            borderBottomWidth:1,
                            flex:1,
                        }}/>
                    </View>

                    <View
                        style={{
                            flexDirection:"row",
                            justifyContent:"center",
                            paddingTop:15,
                            marginBottom:10
                        }}
                    >
                        <Image
                            source={{
                                uri:"https://suotepower.com.mx/wp-content/uploads/2019/07/google-logo.png"
                            }}
                            style={{width:30, height: 30, marginHorizontal:10}}
                        />
                        <Image
                            source={{
                                uri:"http://assets.stickpng.com/images/584ac2d03ac3a570f94a666d.png"
                            }}
                            style={{width:30, height: 30, marginHorizontal:10,}}
                        />
                    </View>
                    
                </View>

            </ScrollView>

            
        </SafeAreaView>
    )
}
