import React, { useState } from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { Image } from 'react-native'
import { useWindowDimensions } from 'react-native'
import { View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

import Carousel, {Pagination} from 'react-native-snap-carousel'
import  Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../themes/colors'
import { styles } from '../themes/globalTheme';
import { StackScreenProps } from '@react-navigation/stack';

interface Slide {
    title: string,
    subTitle: string,
    image : string
}

const items :Slide[] = [
    {
        title:"Title 1",
        subTitle: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita voluptatum eveniet, velit culpa voluptate suscipit deleniti earum quasi sed odit animi pariatur qui facere debitis officiis obcaecati ipsum excepturi optio.",
        image: "https://scontent.fmex27-1.fna.fbcdn.net/v/t1.6435-9/197830215_330949581904119_1279602023048167883_n.png?_nc_cat=103&_nc_rgb565=1&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeEvWpxMjvxstw0y4vwlbzB043u28b4R1zjje7bxvhHXOBV4Cwq0X246Xasi0V8PD4lLp4ATPJjhmdk5Cl0pi-4w&_nc_ohc=aH02sVGZTiMAX_zLgDL&_nc_ht=scontent.fmex27-1.fna&oh=ff582341449cf5dbe51d59e131eb4090&oe=60F0EF37"
    },
    {
        title:"Title 2",
        subTitle: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita voluptatum eveniet, velit culpa voluptate suscipit deleniti earum quasi sed odit animi pariatur qui facere debitis officiis obcaecati ipsum excepturi optio.",
        image:"https://ub.hiringroomcampus.com/assets/media/ub/company_605213804b8e567de143b6e1.png"
    }
]

interface Props extends StackScreenProps<any, any>{}

export const PresentationScreen = ({navigation} : Props) => {

    const {width} = useWindowDimensions();
    const [activeIndex, setActiveIndex] = useState(0)

    const renderItem = (item : Slide) => {
        return(
            <View style={{
                flex:1,
                backgroundColor:"white",
                borderRadius:5,
                padding:40,
                justifyContent:"center",
                
            }}>
                <Image
                    source={{
                        uri:item.image,
                    }}
                    style={{
                        width:"100%",
                        height:300,
                        resizeMode:"center",
                    }}
                />
                <Text style={styles.presentationTitle}>{item.title}</Text>
                <Text style={styles.presentationSubTitle}>{item.subTitle}</Text>
            </View>
            
        )
    }

    const {} = useSafeAreaInsets()
    return (
        <View
            style={{
                flex:1,
                paddingTop:50,
            }}
        >
            <Carousel
                data={items}
                renderItem={({item}) : any => renderItem(item)}
                sliderWidth={width}
                itemWidth={width}
                layout="default"
                onSnapToItem={(index) => {
                    setActiveIndex(index)
                }}
            />
            <View style={{
                ...styles.globalMargin,
                flexDirection:"row",
                justifyContent:"space-between"
            }}>
                <Pagination
                    dotsLength={items.length}
                    activeDotIndex={activeIndex}
                    dotStyle={{
                        width:10,
                        height:10,
                        borderRadius:10,
                        backgroundColor:colors.blue
                    }}
                />
                {
                    (activeIndex === items.length -1)
                    &&
                    <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            backgroundColor:colors.blue,
                            flexDirection:"row",
                            width:140  
                        }}
                        onPress={() => {navigation.navigate("Welcome")}}
                    >
                        <Text style={styles.buttonText}>Continue </Text>
                        <Icon
                         name="chevron-forward-outline"
                         style={{
                             ...styles.buttonText,
                             fontSize:30
                            }}
                        />
                    </TouchableOpacity>
                }
                
            </View>
            
        </View>
    )
}
