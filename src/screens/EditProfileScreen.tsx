import React, { useContext, useEffect, useState } from 'react'
import { TouchableOpacity, View, ActivityIndicator } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { ScrollView } from 'react-native';
import { FormInput, FormType } from '../components/FormInput';
import { Text } from 'react-native';
import { styles } from '../themes/globalTheme';
import { SafeAreaView } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import { formState, useForm } from '../hooks/useForm';
import { EditFields} from '../interface/EditInterface';
import { CameraButton } from '../components/CameraButton';
import { colors } from '../themes/colors';
import DatePicker from "react-native-date-picker"
import { DateInput } from '../components/DateInput';

interface Props extends StackScreenProps<any, any>{};



export const EditProfileScreen = ({navigation} : Props) => {
    const {authState, edit} = useContext(AuthContext)
    const sendFields :  EditFields={
        firstName: authState.userData.firstName,
        lastName: authState.userData.lastName,
        phone: authState.userData.phone,
        dob: authState.userData.dob
    }
    const {sucess, setsucess, put, setform, form, response} = useForm<EditFields, EditFields>("/"+authState.userData.id.toString(), sendFields, {Authorization : authState.token})
    const [date, setdate] = useState(new Date())


    if(sucess === formState.completed){
        
        edit(response!)
        setsucess(formState.await)
    }

    return (
        <SafeAreaView style={{flex:1}}>
            <ScrollView style={{flex: 1, marginBottom: 0}}>
                <View style={[styles.globalMargin,{flex: 1, alignItems: "stretch", justifyContent: "center"}]}>
                    <View style={{flex : 1}}>
                        <Text style={{
                            ...styles.settingsTitle,
                            marginBottom:10
                        }}>
                            Edit Profile
                        </Text>
                        <CameraButton
                            launchResp={() => {}}
                        />
                        <View style={{
                            marginTop: 15
                        }}>
                            <FormInput
                                placeholder="First Name"
                                onChangeText={(value) => setform({...form, firstName: value})}
                                formType = {FormType.name}
                                defaultValue={(authState.userData.firstName)}
                            />
                            <FormInput
                                placeholder="Last Name"
                                onChangeText={(value) => setform({...form, lastName: value})}
                                formType = {FormType.name}
                                defaultValue={authState.userData.lastName}
                            />
                            <FormInput
                                placeholder="Phone Number"
                                onChangeText={(value) => setform({...form, phone: value})}
                                formType = {FormType.number}
                                defaultValue={authState.userData.phone}
                            />
                            <DateInput
                                placeholder={authState.userData.dob!}
                                onChangeText={() => {}}
                                initialDate={authState.userData.dob!}
                            />
                        </View>
                        
                    </View>
                    
                </View>
                
            </ScrollView>
            
            {
                sucess != formState.loading 
                ? <View style={[styles.globalMargin, {marginVertical:20}]}>
                        {(sucess != formState.await) && <Text>{sucess === formState.completed
                            ? "Datos actualizados correctamente"
                            : "Error, revisa tus datos"
                        }</Text>}
                    <TouchableOpacity 
                        style={{
                            ...styles.Button,
                            width:"100%",
                            backgroundColor:colors.red,
                            marginBottom: 0
                        }}
                        onPress={() => put()}
                    >
                        <Text style={styles.buttonText}>Update</Text>
                    </TouchableOpacity>
                    
                </View>
                :<ActivityIndicator size={30} color="grey" style={{bottom: 40}}/>
            }
            
            
            
        </SafeAreaView>
        
    )
}
