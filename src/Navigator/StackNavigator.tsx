import {createStackNavigator} from '@react-navigation/stack'
import React, { useContext } from 'react'
import { View, TouchableOpacity } from 'react-native';
import { Welcome } from '../screens/Welcome';
import { Login } from '../screens/Login';
import { Signup } from '../screens/Signup';
import { ResetPasswordScreen } from '../screens/ResetPasswordScreen';
import { TermsScreen } from '../screens/TermsScreen';
import { HomeScreen } from '../screens/HomeScreen';
import { SettingsScreen } from '../screens/SettingsScreen';
import { AuthContext } from '../context/AuthContext';
import { StackActions } from '@react-navigation/native';
import { EditProfileScreen } from '../screens/EditProfileScreen';
import { ChangePasswordScreen } from '../screens/ChangePasswordScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import { LoadingScreen } from '../screens/LoadingScreen';
import { PresentationScreen } from '../screens/PresentationScreen';

const Stack = createStackNavigator();

export type RootStackParams = {
    Welcome: undefined,
    Login: undefined,
    Signup: undefined,
}


export const StackNavigator = () => {

    const {authState} = useContext(AuthContext)
    return(
        <Stack.Navigator
            initialRouteName={ (authState.token!= "") ? "HomeScreen" : "PresentationScreen"}
            screenOptions={{
                headerTransparent:true,

                headerStyle:{
                    elevation:0,
                    height:45,
                    backgroundColor:"rgba(0,0,0,0)"
                },
                cardStyle:{
                    backgroundColor: "white"
                },
                title:"",
                headerLeftContainerStyle:{
                    alignItems:"flex-start",
                    justifyContent:"flex-end"
                },
                headerBackImage:() => 
                        <Icon 
                            name="arrow-back" 
                            size={20} 
                            color="black"
                            style={{
                                marginHorizontal:12,
                            }}
                        />
            }}
        >
            
            
            {
            (authState.token!= "") 
                ? 
                <>
                    <Stack.Screen name="HomeScreen" component={HomeScreen}/>
                    <Stack.Screen name="SettingsScreen" component={SettingsScreen}/>
                    <Stack.Screen name="EditProfileScreen" component={EditProfileScreen}/>
                    <Stack.Screen name="ChangePasswordScreen" component={ChangePasswordScreen}/>
                </>
                :
                <>
                    <Stack.Screen name="Welcome" component={Welcome}/>
                    <Stack.Screen name="Login" component={Login} options={({}) =>  ({})}/>
                    <Stack.Screen name="Signup" component={Signup}/>
                    <Stack.Screen name="ResetPasswordScreen" component={ResetPasswordScreen}/>
                    <Stack.Screen name="LoadingScreen" component={LoadingScreen}/>
                    <Stack.Screen name="PresentationScreen" component={PresentationScreen}/>
                </>
            }
            <Stack.Screen name="TermsScreen" component={TermsScreen}/>
        </Stack.Navigator>
    )

}