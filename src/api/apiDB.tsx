import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';

export const apiDB = axios.create({
    baseURL: "https://api.ccdemo-development.creativecoefficient.net/1.0/user"

})

apiDB.interceptors.request.use(

    async(config) => {
        const token = await AsyncStorage.getItem("token")
        if(token){
            config.headers.Authorization = token
        }
        return config
    }

)