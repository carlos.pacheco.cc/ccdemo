import React, { useRef, useState } from "react"
import { Platform } from "react-native";
import { apiDB } from '../api/apiDB';
import { AllUsers } from "../interface/LoginInterface";

enum formData{
    email = "email",
    password = "password",
    firstName = "firstName",
    lastName = "lastName",
    phone = "phone",
    avatar= "avatar",
    clientType = "clientType"
}



export enum formState {
    await,
    error,
    completed,
    finished,
    loading
}



export const useForm = <dataType extends {}, dataResp extends {}>(endPoint : string, data : dataType, headers : {} = {}) => {
    const [sucess, setsucess] = useState<formState>(formState.await)
    const [response, setresponse] = useState<dataResp>()
    const [form, setform] = useState(data)

    const checkForm = <fieldType extends {}>(text:fieldType, info:string, input : formData) => {
        setform({...form, [input] : text})
    }

    const get =  async  () : Promise<dataResp | false> => {
        setsucess(formState.loading)
        const promise = await apiDB.get<dataResp>(endPoint, {
            headers: {
                ...headers,
                "Content-Type": "application/json",
            }
        })
        const [promiseResp] = await Promise.all([promise])
        if(promiseResp.status === 200){
            setresponse(promiseResp.data)
            setsucess(formState.completed)
            return promiseResp.data
        }
        else{
            return false
        }
        
        
    }

    const post = async () => {
        setsucess(formState.loading)
        const postReq = await apiDB.post<dataResp>(endPoint, JSON.stringify(form), {
            headers: {
                ...headers,
                "Content-Type": "application/json",
            }
        })
        .then((resp) =>{
            setresponse(resp.data)
            setsucess(formState.completed)
        })
        .catch((resp) => {
            setresponse(resp.response.request._response)
            setsucess(formState.error)
            
        }) 
        
    }

    const put = async () => {
        setsucess(formState.loading)
        const putReq = await apiDB.put<dataResp>(endPoint, (form), {
            headers: {"Content-Type": "application/json",
                ...headers,
            }
        })
        .then((resp) => {
            setresponse(resp.data)
            setsucess(formState.completed)
        }).catch((resp) => {
            setresponse(resp.response.request._response)
            setsucess(formState.error)
        })
    }
    
    return(
        {
            formData, 
            form, 
            checkForm, 
            response, 
            sucess, 
            setsucess, 
            setform,
            put,
            post,
            get
        }
    )
}