import { LoginData, LoginResp, LoginFields } from '../interface/LoginInterface';
import { AuthState, authInitialState } from '../context/AuthContext';
import React from 'react';
import { useState } from 'react';
import { EditFields, EditResponse } from '../interface/EditInterface';


type AuthAction = 
| {type: "Login", payload : LoginResp}
| {type: "Edit", payload: EditFields}
| {type: "LoggedIn", payload: AuthState}
| {type: "Logout"}

export const useAuth = (state : AuthState, action: AuthAction) : AuthState => {
    switch (action.type) {
        case "Login":
            return{
                ...state,
                userData: action.payload.data.user,
                session: action.payload.data.session,
                token: action.payload.data.session.tokenType + " " + action.payload.data.session.accessToken
            }
            break;
        case "Edit":
            return{
                ...state,
                userData: {
                    ...state.userData,
                    firstName: action.payload.firstName,
                    lastName: action.payload.lastName,
                    phone: action.payload.phone,
                    dob: action.payload.dob
                }
            }
            break;
        case "LoggedIn":
            return{
                ...state,
                userData: action.payload.userData,
                token: action.payload.token
            }
            break
        case "Logout":
            return{
                ...authInitialState
            }
            break
            return state;
    }
}
