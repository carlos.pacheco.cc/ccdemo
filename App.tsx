import "react-native-gesture-handler"
import React from 'react'
import { View } from 'react-native';
import { StackNavigator } from './src/Navigator/StackNavigator';
import { Welcome } from './src/screens/Welcome';
import { styles } from './src/themes/globalTheme';
import {NavigationContainer} from "@react-navigation/native"
import { AuthProvider } from "./src/context/AuthContext";
import { StatusBar } from "react-native";

export const App = () => {
  return (
    <NavigationContainer>
      <AppState>
        <StackNavigator/>
      </AppState>
    </NavigationContainer>
  )
}

const AppState = ({children} : any) => {
  return (
    <AuthProvider>
      {children}
    </AuthProvider>
  )
}

 export default App;
